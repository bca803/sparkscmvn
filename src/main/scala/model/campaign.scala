package model

import scala.math.Ordered.orderingToOrdered

case class Campaign(initials: String, level1: Option[String], level2: Option[String], level3: Option[String],
                    level4: Option[String], level5: Option[String], key: String) extends Ordered[Campaign]
{
  override def compare(that: Campaign): Int = (this.level3.getOrElse(""), this.level1.getOrElse("")).compare(that.level3.getOrElse(""), that.level1.getOrElse(""))
}

case class CampaignL(list: List[Option[String]])
{
  val key =  generateKey
  val campaign = generateCampaign

  def generateKey: String = list
    .slice(1, list.size)
    .map(_.get)
    .filter(_.nonEmpty)
    .mkString("-")

  def generateCampaign: Campaign = Campaign(list(0).get, list(1), list(2), list(3), list(4), list(5), key)

  //override def toString: String = "CampaignL: " + list + " with key= " + key

}
