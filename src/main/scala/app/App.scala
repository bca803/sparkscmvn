package app

import java.nio.file.Paths

import utils.VultLog._
import model.{Campaign, CampaignL}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object App
{
  def main(args: Array[String]) {

    log("start app", "")
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val spark = SparkSession
      .builder.config(new SparkConf().setMaster("local"))
      .appName("Campaign")
      .getOrCreate()

    val lines: RDD[String] = spark.sparkContext.textFile(getPath(args(0)))
    p(lines, "lines")

    val campaignsL: RDD[CampaignL] = lines.map(line => line.split(" ").toList)
      .flatMap(l => getCampaigns(l))
    p(campaignsL, "campaignsL")

    val campaignL_keyExtraction = campaignsL.map((cL) => (cL.key, cL))
    p(campaignL_keyExtraction, "campaignL_keyExtraction")

    val filter1 = campaignL_keyExtraction.filter(c => c._2.list(0).get == "bca")
    val filter2 = campaignL_keyExtraction.filter(c => c._2.list(0).get == "mca")

    p(filter1, "filter1")
    p(filter2, "filter2")

    val campaignL_reduceByKey = campaignL_keyExtraction
      .reduceByKey((campaignx, campaigny) => campaignx)
    p(campaignL_reduceByKey, "campaignL_reduceByKey")

    val campaignL_sortByKey = campaignL_reduceByKey
      .sortByKey()
    p(campaignL_sortByKey, "campaignL_sortByKey")

    val campaign_sortByKey: RDD[(String, Campaign)] = campaignL_sortByKey
      .mapValues(cL => cL.campaign)
    p(campaign_sortByKey, "campaign_sortByKey")


    val campaigns = campaign_sortByKey
      .values

    val campaign_sortBy_level3_level1: RDD[Campaign] = campaigns
      .sortBy(c => c, true)
    p(campaign_sortBy_level3_level1, "campaign_sortBy_level3_level1")
    log("end app", "")
  }

  def p(rdd: RDD[_], name: String) = rdd.foreach(log(name, _))

  def getCampaigns(list: List[String]): List[CampaignL] =
  {
    val listWithoutBlanks =  list.filter(s => !s.isEmpty)
    val withParentsList = getWithParentLists(listWithoutBlanks)
    withParentsList.map((l: List[Option[String]]) => CampaignL(l))
  }

  def getWithParentLists(list: List[String]): List[List[Option[String]]] =
  {
    (2 to list.size).map((n: Int) => list.take(n))
      .map((l: List[String]) => stringListToOptional(l, 5))
      .toList
  }

  def stringListToOptional(list: List[String], n: Int): List[Option[String]] =
  {
    val optPatternList = (0 to n-1).map((m: Int) => Some("")).toList
    val optValuesList = list.map((s: String) => Option(s))
    (optValuesList ::: optPatternList).take(n+1)
  }

  def getPath(arg: String): String = {
    Paths.get("").toAbsolutePath.toString + "/" + arg
  }
}
